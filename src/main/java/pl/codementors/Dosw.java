package pl.codementors;

public class Dosw {

    private String okresPracy;

    private String firma;

    private String stanowisko;

    public Dosw(String stanowisko, String drukarz) {
    }

    public Dosw(String okresPracy, String firma, String stanowisko) {
        this.okresPracy = okresPracy;
        this.firma = firma;
        this.stanowisko = stanowisko;
    }

    public String getOkresPracy() {
        return okresPracy;
    }

    public void setOkresPracy(String okresPracy) {
        this.okresPracy = okresPracy;
    }

    public String getFirma() {
        return firma;
    }

    public void setFirma(String firma) {
        this.firma = firma;
    }

    public String getStanowisko() {
        return stanowisko;
    }

    public void setStanowisko(String stanowisko) {
        this.stanowisko = stanowisko;
    }
}
