package pl.codementors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;



@WebServlet(urlPatterns = "/zuraw")
public class cvServlet extends HttpServlet {


        public static final String ZURAWIK = "\n" +
                "                               .....\n" +
                "                           .e$$$$$$$$$$$$$$e.\n" +
                "                          z$$ ^$$$$$$$$$$$$$$$$$.\n" +
                "                        .$$$* J$$$$$$$$$$$$$$$$$$$e\n" +
                "                       .$'  .$$$$$$$$$$$$$$$$$$$$$$*-\n" +
                "                      .$  $$$$$$$$$$$$$$$$***$$  .ee*\n" +
                "        z**$$        $$r ^**$$$$$$$$$** .e$$$$$$**\n" +
                "       * -ee$$      4$$$$.         .ze$$$$$**\n" +
                "       4 z$$$$$      $$$$$$$$$$$$$$$$$$$$$\n" +
                "       $$$$$$$$     .$$$$$$$$$$$**$$$$**\n" +
                "      z$$*    $$     $$$$P***     J$*$$c\n" +
                "      $$*      $$F   .$$$          $$ ^$$\n" +
                "      $$        *$$c.z$$$          $$   $$\n" +
                "      $P          $$$$$$$          4$F   4$\n" +
                "      dP            *$$$*           $$    '$r\n" +
                "      .$                            J$'     $'\n" +
                "       $                             $P     4$\n" +
                "       F                            $$      4$\n" +
                "                                    4$%      4$\n" +
                "                                    $$       4$\n" +
                "                                    d$'       $$\n" +
                "                                    $P        $$\n" +
                "                                    $$         $$\n" +
                "                                    4$%         $$\n" +
                "                                    $$          $$\n" +
                "                                    d$           $$\n" +
                "                                 sss$F         sss33\n";


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.getOutputStream().println(ZURAWIK);
    }
}


