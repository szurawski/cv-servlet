package pl.codementors;

import java.util.List;

/**
 * Just a book.
 *
 * @author psysiu
 */
public class Cv {


    private String imie;

    private String nazwisko;

    private String tel;

    private List<Dosw> doswiadczenie;

    private List<Edu> edukacja;

    private List<String> umiejetnosci;

    private List<String> zainteresowania;


    public Cv() {
    }

    public Cv(String imie, String nazwisko, String tel, List<Dosw> doswiadczenie, List<Edu> edukacja, List<String> umiejetnosci, List<String> zainteresowania) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.tel = tel;
        this.doswiadczenie = doswiadczenie;
        this.edukacja = edukacja;
        this.umiejetnosci = umiejetnosci;
        this.zainteresowania = zainteresowania;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public List<String> getUmiejetnosci() {
        return umiejetnosci;
    }

    public void setUmiejetnosci(List<String> umiejetnosci) {
        this.umiejetnosci = umiejetnosci;
    }

    public List<String> getZainteresowania() {
        return zainteresowania;
    }

    public void setZainteresowania(List<String> zainteresowania) {
        this.zainteresowania = zainteresowania;
    }

    public List<Dosw> getDoswiadczenie() {
        return doswiadczenie;
    }

    public void setDoswiadczenie(List<Dosw> doswiadczenie) {
        this.doswiadczenie = doswiadczenie;
    }

    public List<Edu> getEdukacja() {
        return edukacja;
    }

    public void setEdukacja(List<Edu> edukacja) {
        this.edukacja = edukacja;
    }

    public void init() {
    }
}