package pl.codementors;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class Cvs {

    private Cv cv;


    @PostConstruct
    public void init() {
        List<Dosw> dosw = new ArrayList<>();
        dosw.add(new Dosw("2008-2010","dotMedia","drukarz"));
        dosw.add(new Dosw("2010-2015","matys","drukarz"));
        List<Edu> edukacja = new ArrayList<>();
        edukacja.add(new Edu("1998-2006","Sp5 Kraśnik","podstawowe"));
        List<String> zainteresowania = new ArrayList<>();
        zainteresowania.add("sport");
        zainteresowania.add("komputery");
        zainteresowania.add("muzyka");
        List<String> umiejetnosci = new ArrayList<>();
        umiejetnosci.add("szybko biegam na 10m");
        umiejetnosci.add("skacze nisko");
        umiejetnosci.add("turlam się koślawo");
        cv = new Cv("sebastian", "żurawski", "701 702 703", dosw, edukacja, umiejetnosci, zainteresowania);

    }

    public Cv getCv() {
        return cv;
    }
}
