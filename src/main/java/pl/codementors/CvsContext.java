package pl.codementors;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class CvsContext implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        if (sce.getServletContext().getAttribute("cv") != null) {
            throw new IllegalStateException("Cv store already exists.");
        }
        Cv cv = new Cv();
        cv.init();
        sce.getServletContext().setAttribute("cv", cv);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
