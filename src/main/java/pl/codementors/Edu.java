package pl.codementors;

public class Edu {

    private String okresNauki;

    private String szkola;

    private String specjalnosc;

    Edu(){

    }

    public Edu(String okresNauki, String szkola, String specjalnosc) {
        this.okresNauki = okresNauki;
        this.szkola = szkola;
        this.specjalnosc = specjalnosc;
    }

    public String getOkresNauki() {
        return okresNauki;
    }

    public void setOkresNauki(String okresNauki) {
        this.okresNauki = okresNauki;
    }

    public String getSzkola() {
        return szkola;
    }

    public void setSzkola(String szkola) {
        this.szkola = szkola;
    }

    public String getSpecjalnosc() {
        return specjalnosc;
    }

    public void setSpecjalnosc(String specjalnosc) {
        this.specjalnosc = specjalnosc;
    }
}
